from django import views
from django.urls import path
from .views import (
    AboutView,
    AnalyticsView,
    CabinetView,
    ItemDetailView,
    CheckoutView,
    HomeView,
    OrderSummaryView,
    add_to_cart,
    remove_from_cart,
    remove_single_item_from_cart,
    view_func,
    pay,
    #cabinet,
    PaymentView,
    AddCouponView,
    RequestRefundView,
    CategoryView,
    ItemCreateView,
    ItemUpdateView,
    ItemDeleteView,
    ContactForm, 
    ContactCreate,
    BusinessCreate
    #success

)

app_name = 'core'

urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('category/<slug>/', CategoryView.as_view(), name='category'),
    path('checkout/', CheckoutView.as_view(), name='checkout'),
    path('order-summary/', OrderSummaryView.as_view(), name='order-summary'),
    path('product/<slug>/', ItemDetailView.as_view(), name='product'),
    path('add-to-cart/<slug>/', add_to_cart, name='add-to-cart'),
    path('add-coupon/', AddCouponView.as_view(), name='add-coupon'),
    path('remove-from-cart/<slug>/', remove_from_cart, name='remove-from-cart'),
    path('remove-item-from-cart/<slug>/', remove_single_item_from_cart,
         name='remove-single-item-from-cart'),
    path('payment/<payment_option>/', PaymentView.as_view(), name='payment'),
    path('checkout/pay', pay, name='pay'),

    path('request-refund/', RequestRefundView.as_view(), name='request-refund'),
    path('about/', AboutView.as_view(), name='about'),
    path('personal_cabinet/itemcreate/', ItemCreateView.as_view(), name='itemcreate'),
    path('personal_cabinet/update/<slug>/', ItemUpdateView.as_view(), name='itemupdate'),
    path('personal_cabinet/delete/<slug>/', ItemDeleteView.as_view(), name='itemdelete'),
    path('personal_cabinet/', CabinetView.as_view(), name='cabinet'),
    path('contact/', ContactCreate.as_view(), name='contact'),
    path('personal_cabinet/add_data', BusinessCreate.as_view(), name='business_data'),
    path('personal_cabinet/analytics', view_func, name='analytics'),

    
    
    #path('success/', success, name='success_page'),

]
