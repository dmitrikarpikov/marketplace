#from itertools import count

import random
import string
import datetime

import stripe
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.views.generic import ListView, DetailView, View, CreateView, UpdateView, DeleteView
from django import http
from qsstats import QuerySetStats
from django.db.models import Avg, Count, Sum
from django.db import connection


from django.urls import reverse_lazy
from django.http import HttpResponse
from django.core.mail import send_mail
from django.contrib.messages.views import SuccessMessageMixin


from .forms import CheckoutForm, CouponForm, RefundForm, PaymentForm, ItemForm, ContactForm, Business_dataForm
from .models import Item, Cabinet, About, OrderItem, Order, Address, Payment, Coupon, Refund, UserProfile, Category, Contact, Business_data

stripe.api_key = settings.STRIPE_SECRET_KEY


def create_ref_code():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=20))


def products(request):
    context = {
        'items': Item.objects.all()
    }
    return render(request, "products.html", context)

def abouts(request):
    context = {
        'abouts': About.objects.all()
    }
    return render(request, "about.html", context)

@login_required
def view_func(request):
    start_date = datetime.date(year=2022, month=5, day=1)
    end_date = datetime.date(year=2022, month=6, day=1)

    #class GAVNO(Order, Payment)
    

    queryset = Order.objects.filter(items__item__creator__username = request.user)

    # считаем количество платежей...
    qsstats = QuerySetStats(queryset, date_field='ordered_date')
    qsstats2 = QuerySetStats(queryset, date_field='ordered_date', aggregate=Sum('payment__amount'))

    # ...в день за указанный период
    values = qsstats.time_series(start_date, end_date, interval='days')
    values2 = qsstats2.time_series(start_date, end_date, interval='days')


    # queryset2 = Order.objects.raw("""
    # select * from Order
    # left join Payment on Order.payment = Payment.payment_id""")
    # qsstats2 = QuerySetStats(queryset2, date_field='ordered_date', aggregate='amount')
    # values2 = qsstats2.time_series(start_date, end_date, interval='days')

    context = {
        'values': values,
        'values2': values2


    }
    return render(request, "analytics.html", context)
     

# def about(request):
#     context = {
#         'about': About.objects.all()
#     }
#     return render(request, "about.html", context)

# def cabinet(request):
#     context = {
#         'cabinet': Cabinet.objects.all()
#     }
#     return render(request, "cabinet.html", context)





def is_valid_form(values):
    valid = True
    for field in values:
        if field == '':
            valid = False
    return valid


class BusinessCreate(SuccessMessageMixin, CreateView):
    model = Business_data
    # fields = ["first_name", "last_name", "message"]
    template_name = "business_data.html"
    success_url = "/"
    form_class = Business_dataForm
    success_message = "Данные обновлены."

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.save(commit=False)
        return super().form_valid(form)


    def form_invalid(self, form):
        return http.HttpResponse(form.errors.as_json())


class ContactCreate(SuccessMessageMixin, CreateView):
    model = Contact
    # fields = ["first_name", "last_name", "message"]
    template_name = "contact.html"
    success_url = "/"
    form_class = ContactForm
    success_message = "Письмо отправлено, ожидайте ответа."

    def form_valid(self, form):
        # Формируем сообщение для отправки
        data = form.data
        form.save()
        #Если все таки не сможешь сделать отправку письма, то просто закомменть и он будет создавать запись в таблице
        #+ выведет сообщение с пересылкой на домашнюю страницу
        subject = f'Сообщение с формы от {data["first_name"]} {data["last_name"]} Почта отправителя: {data["email"]}'
        email(subject, data['message'])
        return super().form_valid(form)


# Функция отправки сообщения
def email(subject, content):
   send_mail(subject,
      content,
      'dmitrikarpikov@gmail.com',
      ['dima.karpikov2001@ya.ru']
   )

# Функция, которая вернет сообщение в случае успешного заполнения формы
#def success(request):
#   return HttpResponse('Письмо отправлено!')

class ItemListView(ListView):
    template_name = "category-page.html"
    paginate_by = 10
    model = Item

    def get_context_data(self, **kwargs):
        # self.item_categories = Item.objects.all()
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['item_categories'] = Category.objects.all()
        return context

    def get_queryset(self):
        self.categories = get_object_or_404(Category, slug=self.kwargs['slug'])
        return Item.objects.filter(categories=self.categories)


class ItemCreateView(CreateView):
    template_name = 'itemform.html'
    form_class = ItemForm
    success_url = '/'

    def form_valid(self, form):
        form.instance.creator = self.request.user
        form.save(commit=False)
        return super().form_valid(form)


    def form_invalid(self, form):
        return http.HttpResponse(form.errors.as_json())


class ItemUpdateView(UpdateView):
    template_name = 'update_item.html'
    form_class = ItemForm
    model = Item
    success_url = '/'

    def form_valid(self, form):
        form.instance.creator = self.request.user
        form.save(commit=False)
        return super().form_valid(form)

    def form_invalid(self, form):
        return http.HttpResponse(form.errors.as_json())

class ItemDeleteView(DeleteView):
    template_name = 'delete_item.html'
    model = Item
    success_url = '/'

    def form_invalid(self, form):
        return http.HttpResponse(form.errors.as_json())


class CategoryView(ListView):
    template_name = "category-page.html"
    paginate_by = 10
    model = Item

    def get_context_data(self, **kwargs):
        #self.item_categories = Item.objects.all()
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['item_categories'] = Category.objects.all()
        return context
        
    def get_queryset(self):
        self.categories = get_object_or_404(Category, slug=self.kwargs['slug'])
        return Item.objects.filter(categories=self.categories)

class CheckoutView(View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            form = CheckoutForm()
            context = {
                'form': form,
                'couponform': CouponForm(),
                'order': order,
                'DISPLAY_COUPON_FORM': True
            }

            shipping_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='S',
                default=True
            )
            if shipping_address_qs.exists():
                context.update(
                    {'default_shipping_address': shipping_address_qs[0]})

            billing_address_qs = Address.objects.filter(
                user=self.request.user,
                address_type='B',
                default=True
            )
            if billing_address_qs.exists():
                context.update(
                    {'default_billing_address': billing_address_qs[0]})
            return render(self.request, "checkout.html", context)
        except ObjectDoesNotExist:
            messages.info(self.request, "You do not have an active order")
            return redirect("core:checkout")

    def post(self, *args, **kwargs):
        form = CheckoutForm(self.request.POST or None)
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            if form.is_valid():

                use_default_shipping = form.cleaned_data.get(
                    'use_default_shipping')
                if use_default_shipping:
                    print("Using the defualt shipping address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='S',
                        default=True
                    )
                    if address_qs.exists():
                        shipping_address = address_qs[0]
                        order.shipping_address = shipping_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default shipping address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new shipping address")
                    shipping_address1 = form.cleaned_data.get(
                        'shipping_address')
                    shipping_address2 = form.cleaned_data.get(
                        'shipping_address2')
                    shipping_country = form.cleaned_data.get(
                        'shipping_country')
                    shipping_zip = form.cleaned_data.get('shipping_zip')

                    if is_valid_form([shipping_address1, shipping_country, shipping_zip]):
                        shipping_address = Address(
                            user=self.request.user,
                            street_address=shipping_address1,
                            apartment_address=shipping_address2,
                            country=shipping_country,
                            zip=shipping_zip,
                            address_type='S'
                        )
                        shipping_address.save()

                        order.shipping_address = shipping_address
                        order.save()

                        set_default_shipping = form.cleaned_data.get(
                            'set_default_shipping')
                        if set_default_shipping:
                            shipping_address.default = True
                            shipping_address.save()

                    # else:
                    #     messages.info(
                    #         self.request, "Please fill in the required shipping address fields")

                use_default_billing = form.cleaned_data.get(
                    'use_default_billing')
                same_billing_address = form.cleaned_data.get(
                    'same_billing_address')

                if same_billing_address:
                    billing_address = shipping_address
                    billing_address.pk = None
                    billing_address.save()
                    billing_address.address_type = 'B'
                    billing_address.save()
                    order.billing_address = billing_address
                    order.save()

                elif use_default_billing:
                    print("Using the defualt billing address")
                    address_qs = Address.objects.filter(
                        user=self.request.user,
                        address_type='B',
                        default=True
                    )
                    if address_qs.exists():
                        billing_address = address_qs[0]
                        order.billing_address = billing_address
                        order.save()
                    else:
                        messages.info(
                            self.request, "No default billing address available")
                        return redirect('core:checkout')
                else:
                    print("User is entering a new billing address")
                    billing_address1 = form.cleaned_data.get(
                        'billing_address')
                    billing_address2 = form.cleaned_data.get(
                        'billing_address2')
                    billing_country = form.cleaned_data.get(
                        'billing_country')
                    billing_zip = form.cleaned_data.get('billing_zip')

                    if is_valid_form([billing_address1, billing_country, billing_zip]):
                        billing_address = Address(
                            user="self.request.user",
                            street_address="billing_address1",
                            apartment_address="billing_address2",
                            country="billing_country",
                            zip="billing_zip",
                            address_type='B'
                        )
                        billing_address.save()

                        order.billing_address = billing_address
                        order.save()

                        set_default_billing = form.cleaned_data.get(
                            'set_default_billing')
                        if set_default_billing:
                            billing_address.default = True
                            billing_address.save()

                    # else:
                    #     messages.info(
                    #         self.request, "Please fill in the required billing address fields")

                payment_option = form.cleaned_data.get('payment_option')

                if payment_option == 'S':
                    return redirect('core:payment', payment_option='stripe')
                elif payment_option == 'P':
                    return redirect('core:payment', payment_option='paypal')
                else:
                    messages.warning(
                        self.request, "Invalid payment option selected")
                    return redirect('core:checkout')
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("core:order-summary")


@login_required
def pay(request):
        order = Order.objects.get(user=request.user, ordered=False)
        payment = Payment()
        payment.stripe_charge_id = '11111111111'
        payment.user = request.user
        payment.amount = order.get_total()
        payment.save()       
        order_items = order.items.all()
        order_items.update(ordered=True)
        for item in order_items:
            item.save()

        order.ordered = True
        order.payment = payment
        order.ref_code = create_ref_code()
        order.save()
        return redirect("core:home") 

class PaymentView(View):
    
    

    def get(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        # это заглушка через True должно быть if order.billing_address:
        if True:
            context = {
                'order': order,
                'DISPLAY_COUPON_FORM': False,
                'STRIPE_PUBLIC_KEY' : settings.STRIPE_PUBLIC_KEY
            }
            userprofile = self.request.user.userprofile
            if userprofile.one_click_purchasing:
                # fetch the users card list
                cards = stripe.Customer.list_sources(
                    userprofile.stripe_customer_id,
                    limit=3,
                    object='card'
                )
                card_list = cards['data']
                if len(card_list) > 0:
                    # update the context with the default card
                    context.update({
                        'card': card_list[0]
                    })
            return render(self.request, "payment.html", context)
        else:
            messages.warning(
                self.request, "You have not added a billing address")
            return redirect("core:checkout")

    def post(self, *args, **kwargs):
        order = Order.objects.get(user=self.request.user, ordered=False)
        form = PaymentForm(self.request.POST)
        userprofile = UserProfile.objects.get(user=self.request.user)
        if form.is_valid():
            token = form.cleaned_data.get('stripeToken')
            save = form.cleaned_data.get('save')
            use_default = form.cleaned_data.get('use_default')

            if save:
                if userprofile.stripe_customer_id != '' and userprofile.stripe_customer_id is not None:
                    customer = stripe.Customer.retrieve(
                        userprofile.stripe_customer_id)
                    customer.sources.create(source=token)

                else:
                    customer = stripe.Customer.create(
                        email=self.request.user.email,
                    )
                    customer.sources.create(source=token)
                    userprofile.stripe_customer_id = customer['id']
                    userprofile.one_click_purchasing = True
                    userprofile.save()

            amount = int(order.get_total() * 100)

            try:

                if use_default or save:
                    # charge the customer because we cannot charge the token more than once
                    charge = stripe.Charge.create(
                        amount=amount,  # cents
                        currency="rub",
                        customer=userprofile.stripe_customer_id
                    )
                else:
                    # charge once off on the token
                    charge = stripe.Charge.create(
                        amount=amount,  # cents
                        currency="rub",
                        source=token
                    )

                # create the payment
                payment = Payment()
                payment.stripe_charge_id = charge['id']
                payment.user = self.request.user
                payment.amount = order.get_total()
                payment.save()

                # assign the payment to the order

                order_items = order.items.all()
                order_items.update(ordered=True)
                for item in order_items:
                    item.save()

                order.ordered = True
                order.payment = payment
                order.ref_code = create_ref_code()
                order.save()

                messages.success(self.request, "Your order was successful!")
                return redirect("/")

            except stripe.error.CardError as e:
                body = e.json_body
                err = body.get('error', {})
                messages.warning(self.request, f"{err.get('message')}")
                return redirect("/")

            except stripe.error.RateLimitError as e:
                # Too many requests made to the API too quickly
                messages.warning(self.request, "Rate limit error")
                return redirect("/")

            except stripe.error.InvalidRequestError as e:
                # Invalid parameters were supplied to Stripe's API
                print(e)
                messages.warning(self.request, "Invalid parameters")
                return redirect("/")

            except stripe.error.AuthenticationError as e:
                # Authentication with Stripe's API failed
                # (maybe you changed API keys recently)
                messages.warning(self.request, "Not authenticated")
                return redirect("/")

            except stripe.error.APIConnectionError as e:
                # Network communication with Stripe failed
                messages.warning(self.request, "Network error")
                return redirect("/")

            except stripe.error.StripeError as e:
                # Display a very generic error to the user, and maybe send
                # yourself an email
                messages.warning(
                    self.request, "Something went wrong. You were not charged. Please try again.")
                return redirect("/")

            except Exception as e:
                # send an email to ourselves
                messages.warning(
                    self.request, "A serious error occurred. We have been notifed.")
                return redirect("/")

        messages.warning(self.request, "Invalid data received")
        return redirect("/payment/stripe/")


class HomeView(ListView):
    model = Item
    paginate_by = 10
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        #self.item_categories = Item.objects.all()
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['item_categories'] = Category.objects.all()
        return context

class AboutView(ListView):
    model = About
    paginate_by = 10
    template_name = "about.html"

class AnalyticsView(ListView):
    model = Order
    paginate_by = 10
    template_name = "analytics.html"

    # def get_queryset(self):
    #     #self.categories = get_object_or_404(Category, slug=self.kwargs['slug'])
    #     return Order.items.filter(items=self.request.user)

class CabinetView(ListView):
    #model = Cabinet
    model = Item
    #paginate_by = 10
    template_name = "cabinet.html"
    paginate_by = 10
    # template_name = "home.html"

    def get_context_data(self, **kwargs):
        #self.item_categories = Item.objects.all()
        # Call the base implementation first to get a context
        context = super().get_context_data(**kwargs)
        # Add in the publisher
        context['data_about'] = Business_data.objects.filter(user=self.request.user).last().data_about
        context['email'] = Business_data.objects.filter(user=self.request.user).last().email
        return context

    def get_queryset(self):
        #self.categories = get_object_or_404(Category, slug=self.kwargs['slug'])
        return Item.objects.filter(creator=self.request.user)

class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'order_summary.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")


class ItemDetailView(DetailView):
    model = Item
    template_name = "product.html"


@login_required
def add_to_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            if order.items.filter(item__slug=item.slug).get().quantity >= item.quantity:
                messages.info(request, "Maximum quantity reached. We don't have any more of this in stock.")
                return redirect("core:order-summary")
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "This item quantity was updated.")
            return redirect("core:order-summary")
        else:
            order.items.add(order_item)
            messages.info(request, "This item was added to your cart.")
            return redirect("core:order-summary")
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.info(request, "This item was added to your cart.")
        return redirect("core:order-summary")


@login_required
def remove_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            messages.info(request, "This item was removed from your cart.")
            return redirect("core:order-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("core:product", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("core:product", slug=slug)


@login_required
def remove_single_item_from_cart(request, slug):
    item = get_object_or_404(Item, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        # check if the order item is in the order
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
            messages.info(request, "This item quantity was updated.")
            return redirect("core:order-summary")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("core:product", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("core:product", slug=slug)


def get_coupon(request, code):
    try:
        coupon = Coupon.objects.get(code=code)
        return coupon
    except ObjectDoesNotExist:
        messages.info(request, "This coupon does not exist")
        return redirect("core:checkout")


class AddCouponView(View):
    def post(self, *args, **kwargs):
        form = CouponForm(self.request.POST or None)
        if form.is_valid():
            try:
                code = form.cleaned_data.get('code')
                order = Order.objects.get(
                    user=self.request.user, ordered=False)
                order.coupon = get_coupon(self.request, code)
                order.save()
                messages.success(self.request, "Successfully added coupon")
                return redirect("core:checkout")
            except ObjectDoesNotExist:
                messages.info(self.request, "You do not have an active order")
                return redirect("core:checkout")


class RequestRefundView(View):
    def get(self, *args, **kwargs):
        form = RefundForm()
        context = {
            'form': form
        }
        return render(self.request, "request_refund.html", context)

    def post(self, *args, **kwargs):
        form = RefundForm(self.request.POST)
        if form.is_valid():
            ref_code = form.cleaned_data.get('ref_code')
            message = form.cleaned_data.get('message')
            email = form.cleaned_data.get('email')
            # edit the order
            try:
                order = Order.objects.get(ref_code=ref_code)
                order.refund_requested = True
                order.save()

                # store the refund
                refund = Refund()
                refund.order = order
                refund.reason = message
                refund.email = email
                refund.save()

                messages.info(self.request, "Your request was received.")
                return redirect("core:request-refund")

            except ObjectDoesNotExist:
                messages.info(self.request, "This order does not exist.")
                return redirect("core:request-refund")
