import email
from email import message
from tkinter import Widget
from django import forms
from django.forms import CharField, EmailField, ModelForm
from django.forms import Textarea
from .models import Item, Contact, Business_data
from django_countries.fields import CountryField
from django_countries.widgets import CountrySelectWidget
from django.core.exceptions import ValidationError




PAYMENT_CHOICES = (
    ('S', 'Stripe'),
    # ('P', 'PayPal')
)
COUNTRIES_ONLY = ['RU', 'BR']


class ContactForm(ModelForm):

    class Meta:
        # Определяем модель, на основе которой создаем форму
        model = Contact
        # # Поля, которые будем использовать для заполнения
        # first_name = forms.CharField(required=False)
        # last_name = forms.CharField(required=False)
        # email = forms.CharField(required=False)
        # message = forms.CharField(required=False)




        
        fields = ['first_name', 'last_name', 'email', 'message']
        widgets = {
            # 'first_name': CharField(
            #     attrs={
            #         'placeholder': 'Имя'
            #     }
            # ),
            # 'last_name': CharField(
            #     attrs={
            #         'placeholder': 'Фамилия'
            #     }
            # ),
            # 'first_name': EmailField(
            #     attrs={
            #         'placeholder': 'Почта'
            #     }
            # ),


            'message': Textarea(
                attrs={
                    'placeholder': 'Напишите тут ваше сообщение',
                    'cols': 80, 
                    'rows': 20
                }
            )
        }


class Business_dataForm(forms.ModelForm):


    class Meta:
        model = Business_data
        
        exclude = ['user']
        

    def __init__(self, *args, **kwargs):
        super(Business_dataForm, self).__init__(*args, **kwargs)
        self.fields['data_about'].required = False
        self.fields['email'].required = False
        self.fields['file_name'].required = False
        self.fields['file'].required = False


class ItemForm(forms.ModelForm):


    def clean(self, *args, **kwargs):
        cleaned_data = super().clean(*args, **kwargs)
        if Item.objects.exclude(pk=self.instance.pk).filter(slug=cleaned_data.get('slug')).exists():
            raise ValidationError("Такой slug уже существует!")
        return cleaned_data

    class Meta:
        model = Item
        exclude = ['creator']

        #fields = ["title"]
        
        # widgets = {
        #     'title': Textarea(
        #         attrs={
        #         'placeholder': 'Название товара',
        #         'cols': 40, 
        #         'rows': 1
        #         }
        #     )
        # }


    def __init__(self, *args, **kwargs):
        super(ItemForm, self).__init__(*args, **kwargs)
        self.fields['image'].required = False
        self.fields['image_background'].required = False
        self.fields['title'].widget.attrs.update({'placeholder': 'название товара*', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['price'].widget.attrs.update({'placeholder': 'цена*', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['discount_price'].widget.attrs.update({'placeholder': 'цена с учетом скидки', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['slug'].widget.attrs.update({'placeholder': 'код товара*', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['categories'].widget.attrs.update({'placeholder': 'категория', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['image'].widget.attrs.update({'help_text': 'иконка товара', 'class':'btn btn-primary btn-lg btn-block'})
        self.fields['image_background'].widget.attrs.update({'placeholder': 'картинка товара', 'name':'icon_image', 'class':'btn btn-primary btn-lg btn-block'})
        self.fields['attributes'].widget.attrs.update({'placeholder': 'Описание товара', 'class':'col-lg-4 col-md-6 mb-4'})
        self.fields['quantity'].widget.attrs.update({'placeholder': 'количество товара', 'class':'col-lg-4 col-md-6 mb-4'})



    


                    

    #def clean_slug(self):
    #    slug = self.cleaned_data['slug']
    #    if Item.objects.filter(slug=slug).exists():
    #        raise ValidationError("Такой slug уже существует!")
    #    return slug


class CheckoutForm(forms.Form):
    shipping_address = forms.CharField(required=False)
    shipping_address2 = forms.CharField(required=False)
    shipping_country = CountryField(blank_label='(select country)').formfield(
        required=False,
        widget=CountrySelectWidget(attrs={
            'class': 'custom-select d-block w-100',
        }))
    shipping_zip = forms.CharField(required=False)

    billing_address = forms.CharField(required=False)
    billing_address2 = forms.CharField(required=False)
    billing_country = CountryField(blank_label='(select country)').formfield(
        required=False,
        widget=CountrySelectWidget(attrs={
            'class': 'custom-select d-block w-100',
        }))
    billing_zip = forms.CharField(required=False)

    same_billing_address = forms.BooleanField(required=False)
    set_default_shipping = forms.BooleanField(required=False)
    use_default_shipping = forms.BooleanField(required=False)
    set_default_billing = forms.BooleanField(required=False)
    use_default_billing = forms.BooleanField(required=False)

    payment_option = forms.ChoiceField(
        widget=forms.RadioSelect, choices=PAYMENT_CHOICES)


class CouponForm(forms.Form):
    code = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Promo code',
        'aria-label': 'Recipient\'s username',
        'aria-describedby': 'basic-addon2'
    }))


class RefundForm(forms.Form):
    ref_code = forms.CharField()
    message = forms.CharField(widget=forms.Textarea(attrs={
        'rows': 4
    }))
    email = forms.EmailField()


class PaymentForm(forms.Form):
    stripeToken = forms.CharField(required=False)
    save = forms.BooleanField(required=False)
    use_default = forms.BooleanField(required=False)
