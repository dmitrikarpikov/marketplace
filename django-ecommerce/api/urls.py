from django.urls import path
from rest_framework.authtoken import views
from .views import ItemList, ItemDetail

urlpatterns = [
    path('api/', ItemList.as_view(), name='api'),
    path('api/detail/<slug>/', ItemDetail.as_view(), name='api_detail'),
    path('api-token-auth/', views.obtain_auth_token, name='api_token')
]