from core.models import Item
from rest_framework import serializers


class ItemSerializer(serializers.ModelSerializer):

    #def __init__(self, *args, **kwargs):
    #    many = kwargs.pop('many', True)
    #    super(ItemSerializer, self).__init__(many=many, *args, **kwargs)


    class Meta:
        model = Item
        fields = '__all__'
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }
        read_only_fields = ['image', 'image_background', 'creator']
